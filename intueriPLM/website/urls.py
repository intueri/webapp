from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

from website import Views

urlpatterns = patterns('',

    url(r'^$', TemplateView.as_view(template_name="index.html"), name='index'),
    url(r'^pricing/$', TemplateView.as_view(template_name="pricing.html"), name='pricing'),
    url(r'^contactus/$', TemplateView.as_view(template_name="contactus.html"), name='contactus'),
    url(r'^aboutus/$', TemplateView.as_view(template_name="aboutus.html"), name='aboutus'),
)
