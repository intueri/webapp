from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

from website import Views

urlpatterns = patterns('',

    url(r'^login/$', TemplateView.as_view(template_name="login.html"), name='login'),
)
